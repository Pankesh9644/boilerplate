import { BrowserRouter } from "react-router-dom";
import { Provider } from "./helpers/context";
import "./App.css";
import Router from "./components/Router";

const App = () => {
  return (
    <BrowserRouter>
      <Provider>
        <Router />
      </Provider>
    </BrowserRouter>
  );
};

export default App;
