const ScreenWrapper = ({ children }) => {
  return (
    <div className="screen-wrapper-container">
      <main>
        <div className="screen-wrapper-box">{children}</div>
      </main>
    </div>
  );
};

export default ScreenWrapper;
