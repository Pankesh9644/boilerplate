import React from "react";

const Loader = ({ isLoading }) => {
  if (!(isLoading || isLoading !== 0)) {
    return (
      <div className="loader-container">
        <div className="loader" />
      </div>
    );
  }
};

export default React.memo(Loader);
