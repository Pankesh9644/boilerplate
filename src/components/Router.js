import React from "react";
import { Routes, Route, Navigate, useLocation } from "react-router-dom";

import { constants } from "../helpers/constants";
import { useContext } from "../helpers/context";

import Loader from "./Loader";

const Login = React.lazy(() => import("../screens/Login"));
const Home = React.lazy(() => import("../screens/Home"));
const PageNotFound = React.lazy(() => import("../screens/PageNotFound"));

const SuspenseWrapper = ({ component }) => <React.Suspense fallback={<Loader />}>{component}</React.Suspense>;

const RequireAuth = ({ component }) => {
  const location = useLocation();
  const { token } = useContext();
  const pagePath = location.pathname;
  const route = Object.values(constants.route).filter((value) => value !== "/");
  if (!token) {
    return <Navigate to={constants.route.login} state={{ from: location }} replace />;
  }
  const isPathExists = route.some((item) => pagePath.includes(item.substring(1)));
  if (!isPathExists) {
    return <Navigate to={constants.route.pageNotFound} />;
  }
  return <SuspenseWrapper component={component} />;
};

const Router = () => {
  return (
    <Routes>
      <Route path={constants.route.default} element={<Navigate to={constants.route.home} />} />
      <Route path={constants.route.login} element={<SuspenseWrapper component={<Login />} />} />
      <Route path={constants.route.home} element={<RequireAuth component={<Home />} />} />
      <Route path={constants.route.pageNotFound} element={<SuspenseWrapper component={<PageNotFound />} />} />
      <Route path="*" element={<Navigate to={constants.route.pageNotFound} />} />
    </Routes>
  );
};

export default Router;
