const host = "URL";

export const constants = {
  token: "",
  host,
  route: {
    default: "/",
    pageNotFound: "/404",
    login: "/login",
    signup: "/signup",
    home: "/home",
  },

  url: {},
};
