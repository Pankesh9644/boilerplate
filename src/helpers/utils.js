export default class Utils {
  static isObject = (obj) => (!obj ? false : typeof obj === "function" || typeof obj === "object");

  static isJSONString = (str) => {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  };

  static capitalizeFirstLetter = (str) => {
    if (typeof str === "string") {
      const firstChar = str.charAt(0)?.toUpperCase() ?? "";
      const restOfStr = str.slice(1) ?? "";
      return firstChar + restOfStr;
    }
    return null;
  };

  static notification = ({ type, message }) => {
    // eslint-disable-next-line no-console
    console.log(type, "===========>", message);
  };

  static getSessionVariables = (key) => {
    try {
      const sessionVariables = {};
      for (const item of Object.keys(window.sessionStorage)) {
        const itemValue = window.sessionStorage.getItem(item);
        if (/^([0-9a-zA-Z+/]{4})*(([0-9a-zA-Z+/]{2}==)|([0-9a-zA-Z+/]{3}=))?$/.test(item)) {
          sessionVariables[window.atob(item)] = this.isJSONString(window.atob(itemValue))
            ? JSON.parse(window.atob(itemValue))
            : window.atob(itemValue);
        } else {
          sessionVariables[item] = this.isJSONString(itemValue) ? JSON.parse(itemValue) : itemValue;
        }
      }
      if (key) {
        return sessionVariables?.[key];
      }
      return sessionVariables;
    } catch {
      window.sessionStorage.clear();
      return {};
    }
  };
}
