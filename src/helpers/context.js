import React from "react";
import axios from "axios";

import Utils from "./utils";

import Loader from "../components/Loader";

export const Context = React.createContext({});

export const defaultContextValues = {
  userdata: {},
  userrole: "",
};

export const Provider = ({ children }) => {
  const [loadingCounter, setLoadingCounter] = React.useState(0);
  const [store, updateStore] = React.useState(defaultContextValues);

  const setStore = (data = {}, storeSession = false) => {
    updateStore((prevStore) => ({ ...prevStore, ...data }));
    if (storeSession) {
      for (const key of Object.keys(data)) {
        const value = data?.[key];
        if (value || value === false || value === 0) {
          window.sessionStorage.setItem(window.btoa(key), Utils.isObject(value) ? window.btoa(JSON.stringify(value)) : window.btoa(value));
        } else {
          window.sessionStorage.removeItem(window.btoa(key));
        }
      }
    }
  };

  React.useEffect(() => {
    const sessionVariables = Utils.getSessionVariables();
    for (const key of Object.keys(sessionVariables)) {
      setStore({ [key]: sessionVariables[key] });
    }
  }, []);

  const apiRequest = React.useMemo(
    () =>
      async ({
        method = "GET",
        url = "",
        contentType = "application/json",
        token = "",
        headers = {},
        data = "",
        loading = true,
        error = "",
        success = "",
        onSuccess = () => {},
      }) => {
        if (loading) {
          setLoadingCounter((prevCounter) => prevCounter + 1);
        }
        try {
          const elements_req_obj = {
            method: method?.toUpperCase(),
            url,
            ...((contentType || token || headers) && {
              headers: {
                "Content-Type": contentType ?? "application/json",
                ...(token && {
                  Authorization: token.startsWith("Bearer") ? token : `Bearer ${token}`,
                }),
                ...headers,
              },
            }),
            data: JSON.stringify(data),
          };
          const elements_response = await axios(elements_req_obj);
          if ([200, 201].includes(elements_response.status) && elements_response.data) {
            if (!["GET"].includes(method?.toUpperCase()) && success) {
              Utils.notification({ type: "Success", message: success });
            }
            onSuccess(elements_response);
            if (loading) {
              setLoadingCounter((prevCounter) => prevCounter - 1);
            }
            return elements_response;
          }
        } catch (e) {
          Utils.notification({ type: "Error", message: error ?? e });
          if (loading) {
            setLoadingCounter((prevCounter) => prevCounter - 1);
          }
        }
      },
    [store]
  );

  // eslint-disable-next-line no-console
  console.log("Store ==============>", store);

  const contextValue = React.useMemo(
    () => ({
      store,
      setStore,
      apiRequest,
      loadingCounter,
      token: Utils.getSessionVariables("token"),
    }),
    [store, setStore, apiRequest, loadingCounter]
  );

  return (
    <Context.Provider value={contextValue}>
      <Loader isLoading={loadingCounter} />
      {children}
    </Context.Provider>
  );
};

export const useContext = () => React.useContext(Context);
