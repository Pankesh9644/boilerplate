import Utils from "../helpers/utils";
import { useContext } from "../helpers/context";
import { constants } from "../helpers/constants";

import ScreenWrapper from "../components/ScreenWrapper";

const Login = () => {
  const { setStore } = useContext();

  return (
    <ScreenWrapper className="App">
      <h1>{Utils.capitalizeFirstLetter("login")}</h1>
      <br />
      <br />
      <input placeholder="username" type="text" value="adam@123.com" />
      <br />
      <br />
      <input placeholder="password" type="password" value="Adam@123" />
      <br />
      <br />
      <button
        type="button"
        onClick={() => {
          setStore({ username: "adam@123.com", token: "APKJGDTFHGANISAUJS" }, true);
          window.location.replace(constants.route.home);
        }}
      >
        Login
      </button>
    </ScreenWrapper>
  );
};

export default Login;
