const PageNotFound = () => {
  return (
    <div className="page-not-found">
      <div className="page-text">
        <h1>We lost this page</h1>
        <p>We searched high and low but couldn't find what you're looking for. Let's find better place for you to go.</p>
      </div>
      <img src="/assets/images/page404.svg" style={{ width: "40%" }} alt="Page not found" />
    </div>
  );
};

export default PageNotFound;
