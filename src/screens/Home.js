import ScreenWrapper from "../components/ScreenWrapper";

const Home = () => {
  return <ScreenWrapper>Home</ScreenWrapper>;
};

export default Home;
